﻿using Checkers.Enums;
using Checkers.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Checkers.Services
{
	public class GameLogic
	{
        private static GameLogic instance;
        private GameLogicBase gameLogicBase;
        private GameLogicClickCommands clickCommands;
        private GameLogicMovePiece movePieceLogic;

        private GameLogic(ObservableCollection<ObservableCollection<GameSquare>> board, PlayerTurn playerTurn, Winner winner)
        {
            gameLogicBase = new GameLogicBase(board, playerTurn, winner);
            clickCommands = new GameLogicClickCommands(board, playerTurn, winner);
            movePieceLogic = new GameLogicMovePiece(board, playerTurn, winner);
        }

        public static GameLogic Instance
        {
            get
            {
                if (instance == null)
                {
                    throw new Exception("GameLogic is not initialized. Call Initialize first.");
                }
                return instance;
            }
        }

        public static void Initialize(ObservableCollection<ObservableCollection<GameSquare>> board, PlayerTurn playerTurn, Winner winner)
        {
            if (instance == null)
            {
                instance = new GameLogic(board, playerTurn, winner);
            }
            else
            {
                throw new Exception("GameLogic is already initialized.");
            }
        }

        public void ResetGame()
        {
            clickCommands.ResetGame();
        }

        public void SaveGame()
        {
            clickCommands.SaveGame();
        }

        public void LoadGame()
        {
            clickCommands.LoadGame();
        }

        public void ClickPiece(GameSquare square)
        {
            clickCommands.ClickPiece(square);
        }

        public void MovePiece(GameSquare square)
        {
            movePieceLogic.MovePiece(square);
        }
    }
}
